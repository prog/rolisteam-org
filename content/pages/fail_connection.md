Title: Fail connection
Date: 2017-06-11 10:20
slug: ipfailed
status: hidden
lang: en

<span style="color:red;">Connection failed!</span>  

Please, take a look to our [documentation](https://doc.rolisteam.org) or the [tutorial]({filename}14_tutorialList.md)

<form action="/php/test_ip.php" method="post">
<table>
<tr><td>Address : </td><td><input name="address" id="address" /></td></tr>
<tr><td>Port : </td><td><input name="port" id="port" value="6660" /></td></tr>
<tr><td></td><td><input type="submit" value="Try to connect"/></td></tr>
</table>
