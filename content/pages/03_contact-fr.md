title: Nous contacter
date: 2017-06-11 10:20
slug: contact
status: hidden
lang: fr

Il existe plusieurs méthodes pour nous contacter ou se tenir informer sur Rolisteam.

* [Forum](http://forum.rolisteam.org/) officiel de Rolisteam
* [Formulaire de contact](contact-form.html)
* [Liste de diffusion](https://mail.kde.org/mailman/listinfo/rolisteam)
* [Remarques, Rapport de bogues, améliorations](https://invent.kde.org/kde/rolisteam/-/issues)
* [Page Facebook](https://www.facebook.com/rolisteam)
* [Discord](https://discord.gg/MrMrQwX)

<iframe src="https://discordapp.com/widget?id=279710365334372352&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>
