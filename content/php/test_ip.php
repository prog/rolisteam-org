<?php

if(!empty($_POST['address']))
{
	/* Lit le port du service WWW. */
	$service_port = $_POST['port'];

	/* Lit l'adresse IP du serveur de destination */
	$address = $_POST['address'];

	/* Crée un socket TCP/IP. */
	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	if ($socket === false) {
	    header("location: /ipfailed");
	} 
	
	$result = socket_connect($socket, $address, $service_port);
	if ($result === false) {
		socket_close($socket);
		header("location: /ipfailed.html");
	} else {
		socket_close($socket);
		header("location: /ipsuccess.html");
	}

}

?>

