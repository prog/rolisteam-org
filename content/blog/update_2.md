date: 2022-07-31 12:00
modified: 2022-07-31 19:20
category: news
slug: update2
lang: en
authors: Rolisteam
summary: July updates about rolisteam development
title: July 2022 - Monthly update #2


Second issue about the progress on rolisteam. 




# 1. Mindmap

When I wrote down a story for a TTRPG game, I always find useful to draw an interaction diagram. It's the best way to get an overview of the story really quickly. 
First, it was on blocknotes, then I wish for a better tool and I designed it.

For a overnight game, this is how a mindmap looks:
![image]({static}/images/updates/002/canada.png)

Now, we decided to add this [rmindmap](https://github.com/obiwankennedy/rmindmap) into rolisteam.
The original tool has received several versions and implementation about diagram style and technology.

## Adding QML mindmap into rolisteam

Let's talk about technical aspect here. The main difference between the stand-alone version and the mindmap inside rolisteam is that in rolisteam, some components have to follow some APi.

![image]({static}/images/updates/002/class_diagram_mediacontrollerbase.svg)

Basically, MediaControllerBase is the low-level api that every media controller shares. You share an image on rolisteam, there is an ImageController which extends from MediaControllerBase. And it is the case for every media: Charactersheet, virtual map, Notes…

MediaControllerBase stores all data required for identifying the media (Uuid, name…) and it also stored data to describe the permission context: the owner, the gamemaster, and does this controller is a remote version or the original one.   


Then, you have the MindMapControllerBase which provided the mindmap API, such as adding nodes, links, packages, remove them, manage the selection and so on. 
This code is mainly from the original stand-alone software `rmindmap`.

Then, the last layer: MindMapController. This class takes in charge any specific behaviour or feature for mindmap into rolisteam. The mindmap is the second media to have a permission selector. Users will be able to share in read-only mode or read-write mode to everyone or to some selected people.


## Features:

* Add node
* Customize node: text, description, tags and style
* Add picture to node (drag and drop or contextual menu)
* Default style 
* Editable link label
* (Activate/Disactive) Automatic spacing 
* Add Packages
* Dark mode
* Realtime small view, the visible part on the mindmap is marked with a blue rectangle
* Export in png
* hide/show link label
* Search for node based on tags, name or description
* Show/Hide children node
* Reparenting
* Add link
* Undo/Redo
* Zoom in/out
* Automatic save
* …

## Demo: From nothing 

![image]({static}/images/updates/002/rmindmap_00.gif)

## Demo: The big one

![image]({static}/images/updates/002/rmindmap_01.gif)



# 2. Image in instant messaging

We implement a small feature on the instant messaging. If you copy/paste a link to image. The url is replaced by the image. 

![image]({static}/images/updates/002/imgInIM.png)

Other urls (to website or other kind of format), the link is clickable. 

# 3. Upnp class

Some users have difficulties to configure their network device at home to host games.   
Then, we create a simple QObject to open network port and forward it to the rolisteam server. 
In rolisteam, the object will only be used on server side.

You can find the implementation [here](https://github.com/obiwankennedy/QtUpnpNat)
It is already integrated into rolisteam. Some improvements can be done, of course. But it works on our side. Feel free to test it on your side and give us feedback.  
There is a unit test to run. The unit test does exactly what we are doing in rolisteam.

# 4. Translation scripts for KDE

As you may know, rolisteam is now part of KDE. We are trying to enjoy that new status. One way to do it, it's to change the way rolisteam is translated.
The KDE project has huge translation team and they are really efficient.   
On project side, when you want your application to be translated. You have to provide a script that extract all texts.
We made this script for DiceParser, and it is now ready for rolisteam.

# 5. Work for August

* Finish Mindmap
* keep working on Vectorial map
* See network status for mindmap and vectorial map
